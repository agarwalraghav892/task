import React from 'react'
import "./Options.css"
import StarIcon from '@mui/icons-material/Star';
const Options = (props) => {
  return (
    <div id='option'>
        <div >
            {props.detail.popular && <div id="popular">
                <StarIcon id="star"/>
                <span> MOST POPULAR </span>
                <StarIcon id="star"/>
            </div>}
            <h2>{props.detail.tittle}</h2>
            <span id='savings'>{props.detail.savings}</span>
            <div id='save'>
                <span id='percentage_saved'>{props.detail.percentage_saved}</span>
                {props.detail.best_result && <span id='best_results'>Best Results</span>}
            </div>
        </div>
        <div id='amount'>
            <div id='offer-price'>{props.detail.offer_price}</div>
            <div id='actual-price'>{props.detail.actual_price}</div>
        </div>
    </div>
  )
}

export default Options