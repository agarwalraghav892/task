import React from 'react'
import image from "../images/Group331.jpg"
import "./Product.css"
import Rating from '@mui/material/Rating';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import Options from './Options.js'
const options=[
    {
        "popular":true,
        "tittle":"1 Months Pack (4 tubes)",
        "savings":"Savings: ₹200",
        "percentage_saved":"38% Saved",
        "best_result":true,
        "offer_price":"₹595",
        "actual_price":"₹795"
    },
    {
        "popular":false,
        "tittle":"3 Months Pack (12 tubes)",
        "savings":"Savings: ₹100",
        "percentage_saved":"12% Saved",
        "best_result":false,
        "offer_price":"₹899",
        "actual_price":"₹999"
    }
]
const Product = () => {
  return (
    <div id='product'>
        <div id='image'>
            <img src={image}></img>
        </div>
        <div id='details'>
            <h2 id='heading'>Everyday rinse and reload</h2>
            <p id='para'>Achieve your dream skin goal with this complete package of skincare produts that will rejuvenate your day.</p>
            <div id='review'>
                <Rating id="rating" name="half-rating-read" defaultValue={4.5} precision={0.5} readOnly/>
                <span id='value'>4.5 </span>
                <span id='review_link'>Click to read reviews</span>
            </div>
            <div id='price'>
                <div id='offer_price'>₹899</div>
                <div id='actual_price'>₹1299</div>
            </div>
            {options.map((option) => (
                <Options detail={option}/>
            ))}
            
            <div id='button-container'>
                <ShoppingCartIcon />
                <button>ADD TO CART</button>
            </div>
        </div>
    </div>
  )
}

export default Product