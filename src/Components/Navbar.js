import React from 'react'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import MenuIcon from '@mui/icons-material/Menu';
import './Navbar.css'

const Navbar = () => {
  return (
    <>
        <div id='Navbar'>
            <MenuIcon />
            <ShoppingCartIcon />
        </div>
        <hr></hr>
    </>
  )
}

export default Navbar