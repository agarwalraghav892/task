import './App.css';
import Navbar from './Components/Navbar';
import Product from './Components/Product.js'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar />
        <Product/>
      </header>
    </div>
  );
}

export default App;
